import React from 'react';
import PropTypes from 'prop-types';

import Content from 'content';

import './richtext.scss';

/**
 * RichText
 * @description [description]
 * @example
  <div id="RichText"></div>
  <script>
	ReactDOM.render(React.createElement(Components.RichText, {
	}), document.getElementById("RichText"));
  </script>
 */
const RichText = props => {
	const baseClass = 'richtext';
	const {className} = props;

	return <Content {...props} className={`${baseClass} ${className}`} />;
};

RichText.defaultProps = {
	content: '',
	className: ''
};

RichText.propTypes = {
	content: PropTypes.string.isRequired,
	className: PropTypes.string
};

export default RichText;
